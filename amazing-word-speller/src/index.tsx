import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { Route,  BrowserRouter } from 'react-router-dom'

import App from './App';
import { AdminMain } from './admin/AdminMain';

const routing = (
  <BrowserRouter>
    <div>
      <Route exact path="/admin" component={AdminMain} />
      <Route exact path="/" component={App} />
    </div>
  </BrowserRouter>
)
ReactDOM.render(routing, document.getElementById('root'))

serviceWorker.unregister();
