import React from "react";
import './Word.css';

interface WordProperties { word: string, letters: string[] }
export class Word extends React.Component<WordProperties> {
    _word: string = "";
    _letters: string[]=[];

    constructor(props: Readonly<WordProperties>) {
        super(props);
        this._word = this.props.word;
        this._letters = this.props.letters;
    }

    render() {
        return (
            <div className="word">
                {
                this._word.split('').map((value: string) => { 
                    if(this._letters.includes(value))
                        return <div className="letter" >{value}</div> 
                    else
                        return <div className="bad-letter" >{value}</div> 
                    })
                }
            </div>
        );
    }

}