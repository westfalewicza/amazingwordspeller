const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB({ apiVersion: '2012-08-10' });

exports.handler = function (event, context, callback) {

	console.log('Received event:', JSON.stringify(event, null, 2));
	var word = event.pathParameters.word;

	if (word === undefined) {
		callback("400 Invalid Input");
	}

	var params = {
		TableName: process.env.WordSuggestionsTable,
		Item: {
			'word': { S: word }
		}
	};

	docClient.putItem(params, function (err, data) {
		if (err) {
			console.log("Error", err);
			callback(err, null);
		} else {
			console.log("Success", data);
			var response = {
				"statusCode": 200,
				headers: {
					"Access-Control-Allow-Methods": "PUT,POST,GET,DELETE,OPTION,HEAD",
					"Access-Control-Allow-Origin": "*",
					"Access-Control-Allow-Headers": "*"
				},
			};
			callback(null, response);
		}
	});
};