import { ApiProxy } from "./ApiProxy"

interface ConnectorListener {
    submitCallback: (score: any, correctWords: string[], nonexistentWords: string[], incorrectWords: string[]) => void
}

export class Connector {
    listener: ConnectorListener;
    _api: ApiProxy = new ApiProxy();

    constructor(listener: Readonly<ConnectorListener>) {
        this.listener = listener;
    }

    submit(words: string[], letters: string[]) {
        var correctWords: string[] = [];
        var badLetterWords: string[] = [];
        var nonexistentWords: string[] = [];

        words.forEach(word => {
            var i;
            for (i = 0; i < word.length; i++) {
                if (!letters.includes(word[i])) {
                    badLetterWords.push(word);
                    break;
                }
                else if (i == word.length - 1) {
                    correctWords.push(word);
                }
            }
        });

        this._api.checkInput(correctWords).then(result => {
            correctWords.forEach(word => {
                if (!result.includes(word)) {
                    nonexistentWords.push(word);
                }
            });
            this.listener.submitCallback(result.length, result, nonexistentWords, badLetterWords);
        });
    }

    report(word: string) {
        this._api.suggestWord(word).then(res => {
                alert(`Admin will check your word ("${word}") and add it if necesary`);
            });
    }
}