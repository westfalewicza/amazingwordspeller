import React from 'react';
import { render } from 'react-dom';
import { ApiProxy } from '../services/ApiProxy';
import './AdminLogin.css';
import '../shared/shared.css';
import { FakeApi } from '../services/FakeApi';

interface LoginProperties {
    authenticatedCallback: (email: string, password: string) => void;
}

export class AdminLogin extends React.Component<LoginProperties> {
    _api = new ApiProxy();
    _prompt = "";
    _email = "";
    _password = "";

    constructor(props: LoginProperties) {
        super(props);
    }

    render() {
        return (
            <div>
                <h1 className="signin-header">Sign in to dictionary admin panel</h1>
                <div>
                    <input className="email-input" type="text" placeholder="email" onChange={this.updateCredsEmail.bind(this)} />
                </div>
                <div>
                    <input className="password-input" type="password" placeholder="password" onChange={this.updateCredsPass.bind(this)} />
                </div>
                <div>
                    <button className="main-btn" onClick={this.authenticate.bind(this)}>Sign in</button>
                </div>
                <div className="prompt-box">{this._prompt}</div>
            </div>);
    }

    updateCredsEmail(event: any) {
        event.persist();
        this._email = event.target.value;
    }

    updateCredsPass(event: any) {
        event.persist();
        this._password = event.target.value;
    }

    authenticate() {
        this._api.authenticate(this._email, this._password).then((isAuthenticated) => {
            if (isAuthenticated) {
                this.props.authenticatedCallback(this._email, this._password);

            }
            else {
                this._prompt = "Please enter valid credentials";
                this.forceUpdate();
            }

        });
    }
}