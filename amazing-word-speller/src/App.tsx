import React from 'react';
import './App.css';
import { Game } from './components/inProgressState/Game';
import { RandomLetterProducer } from './services/producers/RandomLetterProducer';
import { GameState } from './enums/GameState';
import { Info } from './components/awaitingState/Info';
import { Result } from './components/finishedState/Result';
import { Connector } from './services/Connector';

export class App extends React.Component {
  _gameState: GameState = GameState.Awaiting;
  _connector: Connector = new Connector(this);

  _correctWords: string[] = [];
  _nonexistentWords: string[] = [];
  _incorrectWords: string[] = [];
  _score: any;

  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          Amazing 
          Word 
          Speller
        </header>
        <div className="App-content">
          {this.gameStateToElement(this._gameState)}
        </div>
      </div>
    );
  }

  gameStateToElement(gameState : GameState):any {
    switch(gameState) {
      case GameState.Awaiting:
        return <Info onBeginFunction={this.startGame.bind(this)}></Info>
      case GameState.InProgress:
        const producer = new RandomLetterProducer();
        return <Game wordComponentsProducer={producer} 
                timeTolive={10} 
                submitWords={this.submitWords.bind(this)}> </Game>
      case GameState.Finished:
        return <Result playAgain={this.playAgain.bind(this)} 
                reportWord={this.reportWord.bind(this)}
                score={this._score} 
                correctWords={this._correctWords}
                incorrectWords={this._incorrectWords}
                nonexistentWords={this._nonexistentWords}/>;
      default:
        return null;
    }
  }

  startGame(){
    this._gameState = GameState.InProgress;
    this.forceUpdate();
  }

  submitWords(words: string[], letters: string[]){
    console.log("words");
    console.log(words);
    console.log('letters');
    console.log(letters);
    this._connector.submit(words, letters);
  }

  reportWord(word: string) {
    this._connector.report(word);
  }

  submitCallback(score: any, correctWords: string[], nonexistentWords: string[], incorrectWords: string[]) {
    this._score = score;
    this._correctWords = correctWords;
    this._nonexistentWords = nonexistentWords;
    this._incorrectWords = incorrectWords;
    
    this._gameState = GameState.Finished;
    this.forceUpdate();
  }

  playAgain() {
    this._gameState = GameState.Awaiting;
    this.forceUpdate();
  }
}

export default App;