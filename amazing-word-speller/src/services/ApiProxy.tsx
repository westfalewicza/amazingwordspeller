import axios, { AxiosRequestConfig } from 'axios';

export class ApiProxy {
    BASE_URL: string = 'https://l3vwylmc8h.execute-api.us-east-1.amazonaws.com/apiv1';

    authenticate(email: string, password: string): Promise<boolean> {
        console.log('authenticate');
        const params = { "email": email, "password": password  };
        return axios.post(`${this.BASE_URL}/authenticate`, params).then(res => {
            console.log(res);
            return true;
        }).catch(err => {
            console.log(err);
            return false;
        })
    }

    checkInput(words: string[]): Promise<string[]> {
        console.log('checkInput');
        const temp = { words: words };
        return axios.post(`${this.BASE_URL}/words/validate`, temp).then(res => {
            console.log(res);
            return res.data;
        });
    }

    suggestWord(word: string): Promise<boolean> {
        console.log('suggestWord');
        return axios.post(`${this.BASE_URL}/word-suggestions/${word}`).then(res => {
            console.log(res);
            return true;
        });
    }

    getWordSuggestions(email: string, password: string): Promise<string[]> {
        console.log('getWordSuggestions');
        const config: AxiosRequestConfig =  { params: { 
            "email": email,
            "password": password
        }};

        return axios.get(`${this.BASE_URL}/word-suggestions`, config).then(res => {
            console.log(res);
            return res.data;
        });
    }

    addWord(word: string, email: string, password: string): Promise<boolean> {
        console.log('addWord');
        const config: AxiosRequestConfig =  { params: { 
            "email": email,
            "password": password
        }};

        return axios.post(`${this.BASE_URL}/words/${word}`, config).then(res => {
            console.log(res);
            return true;
        });
    }

    deleteWordSuggestion(word: string, email: string, password: string): Promise<boolean> {
        console.log('deleteWordSuggestion');
        const config: AxiosRequestConfig =  { params: { 
            "email": email,
            "password": password
        }};

        return axios.delete(`${this.BASE_URL}/word-suggestions/${word}`,config).then(res => {
            console.log(res);
            return true;
        });
    }
}