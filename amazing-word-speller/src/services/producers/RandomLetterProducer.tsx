import { AbstractWordComponentsProducer } from "./AbstractWordComponentsProducer";

export class RandomLetterProducer extends AbstractWordComponentsProducer{
    getComponents(): string[] {
        return this.shuffle("qwerty uiopasdfghjkl zxcvbnm".replace(/\s/g, "").split('')).slice(0,10);
    }
}