const AWS = require('aws-sdk');
const lambda = new AWS.Lambda({region: 'us-east-1'});

exports.handler = function (event, context, callback) {
    console.log('Received event:'); //Passwords :c
    //console.log('Received event:', JSON.stringify(event, null, 2));
    
    var word = event.pathParameters.word;
    if (word === undefined) {
        callback("400 Invalid Input");
    }

    var lambdaParams = {
      FunctionName: process.env.AuthenticateLambda,
      InvocationType: "RequestResponse",
      Payload: JSON.stringify({
        body: JSON.stringify({
          email: event.queryStringParameters.email,
          password: event.queryStringParameters.password
        })
      })
    };
    
      lambda.invoke(lambdaParams, function(err, lambdaResponse) {
        if (err) {
          console.log(err, err.stack);
          callback(err, null);
        } else {
          var result = JSON.parse(lambdaResponse.Payload);
          console.log('AUTH RESULT', result.statusCode);
          if (result.statusCode != 200)
            callback("401 Authentication failed");
          else 
            deleteWord(word, callback);
        }
      });
};

var deleteWord = function (word, callback) {
    
    const docClient = new AWS.DynamoDB.DocumentClient();
    
    var params = {
        TableName: process.env.WordSuggestionsTable,
        Key: {
            "word": word
        }
    };

    docClient.delete(params, function (err, data) {
        if (err) {
            console.log("Error", err);
            callback(err, null);
        } else {
            console.log("Success", data);
            var response = {
                "statusCode": 200,
				headers: {
					"Access-Control-Allow-Methods": "PUT,POST,GET,DELETE,OPTION,HEAD",
					"Access-Control-Allow-Origin": "*",
					"Access-Control-Allow-Headers": "*"
				},
            };
            callback(null, response);
        }
    });
}
