import { AbstractWordComponentsProducer } from "./AbstractWordComponentsProducer";

export class LetterProducer extends AbstractWordComponentsProducer{
    getComponents(): string[] {
        return "abcdefg".replace(/\s/g, "").split('');
    }
}