const AWS = require("aws-sdk");
const lambda = new AWS.Lambda({ region: "us-east-1" });

exports.handler = function(event, context, callback) {
  console.log("Received event:"); //Passwords :c
  //console.log('Received event:', JSON.stringify(event, null, 2));

  var lambdaParams = {
    FunctionName: process.env.AuthenticateLambda,
    InvocationType: "RequestResponse",
    Payload: JSON.stringify({
      body: JSON.stringify({
        email: event.queryStringParameters.email,
        password: event.queryStringParameters.password
      })
    })
  };

  lambda.invoke(lambdaParams, function(err, lambdaResponse) {
    if (err) {
      console.log(err, err.stack);
      callback(err, null);
    } else {
      var result = JSON.parse(lambdaResponse.Payload);
      console.log("AUTH RESULT", result.statusCode);
      if (result.statusCode != 200) callback("401 Authentication failed");
      else getWords(callback);
    }
  });
};

var getWords = function(callback) {
  const docClient = new AWS.DynamoDB.DocumentClient({ region: "us-east-1" });

  var params = {
    TableName: process.env.WordSuggestionsTable,
    Limit: 30
  };

  docClient.scan(params, function(err, data) {
    if (err) {
      console.error("Error:", err);
      callback(err, null);
    } else {
      console.log("Success", data);

      var words = [];
      data.Items.forEach(function(item) {
        words.push(item.word);
      });

      var response = {
        statusCode: 200,
        body: JSON.stringify(words),
        isBase64Encoded: false,
        headers: {
          "Access-Control-Allow-Methods": "PUT,POST,GET,DELETE,OPTION,HEAD",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "*"
        }
      };

      callback(null, response);
    }
  });
};
