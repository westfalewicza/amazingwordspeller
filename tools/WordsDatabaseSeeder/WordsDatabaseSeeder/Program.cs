﻿using System;
using System.Threading.Tasks;

namespace WordsDatabaseSeeder
{
    class Program
    {
        static void Main(string[] args)
        {
            string tableName = TableNameFromArgs(args);
            if (string.IsNullOrWhiteSpace(tableName))
                return;

            var seeder = new DatabaseSeeder();
            Task seeding =  seeder.SeedTableAsync(tableName);
            seeding.Wait();
        }

        private static string TableNameFromArgs(string[] args)
        {
            if (args.Length == 0)
            {
                return TableNameInteractive();
            }
            else if (args.Length == 1)
            {
                return args[0];
            }
            else
            {
                PrintUsage();
                return null;
            }

            string TableNameInteractive()
            {
                string tableName = "";
                do
                {
                    Console.WriteLine("Enter table name:");
                    tableName = Console.ReadLine();
                }
                while (string.IsNullOrWhiteSpace(tableName));
                return tableName;
            }
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Enter tableName as only argument or launch program with no arguments to run it in interactive");
        }
    }
}

