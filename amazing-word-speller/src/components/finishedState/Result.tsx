import React from "react";
import './Result.css';
import '../../shared/shared.css'

interface ResultProperties {
    playAgain: () => void,
    reportWord: (word: string) => void,
    score: number,
    correctWords: string[],
    nonexistentWords: string[],
    incorrectWords: string[],
}

export class Result extends React.Component<ResultProperties> {
    _alreadyRendered: boolean = false;

    constructor(props: ResultProperties) {
        super(props);
    }

    render() {
        const resultString = "Result: " + this.props.score + (this.props.score == 1 ? " point" : " points");
        return (
            <div className="result-container">
                <p><b>Game finished</b></p>
                <div className="result-points">{resultString}</div>
                <p><b>Correct words:</b></p>
                <div className="result-correctWords">
                    {
                        this.props.correctWords.map((value: string) => { return <p className="result-word">{value}</p> })
                    }
                </div>
                <p><b>Words with illegal letters</b></p>
                <div className="result-incorrectWords"></div>
                {
                    this.props.incorrectWords.map((value: string) => { return <p className="result-word">{value}</p> })
                }
                <p><b>Words not existing in dictionary</b></p>
                <div className="result-nonexistentWords"></div>
                {
                    this.props.nonexistentWords.map((value: string) => {
                        return <p>
                            <span className="result-word">{value}</span>
                            <button className="main-btn" onClick={() => this.props.reportWord(value)}>Report word</button>
                        </p>
                    })
                }
                <button className="main-btn"
                    title="Play again!"
                    onClick={() => this.props.playAgain()}>Play again!</button>
            </div>
        );
    }
}
