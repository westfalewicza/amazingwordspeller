﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;

namespace WordsDatabaseSeeder
{
    internal class DatabaseSeeder
    {
        private readonly WordList _wordList;

        public DatabaseSeeder()
        {
            _wordList = new WordList();
        }

        internal async Task SeedTableAsync(string tableName)
        {
            var client = new AmazonDynamoDBClient(RegionEndpoint.USEast1);

            int i = 0;
            var writeRequestList = new List<WriteRequest>(50);
            foreach (var word in _wordList.Words)
            {
                i++;
                if (i == 25)
                {
                    var dic = new Dictionary<string, List<WriteRequest>>
                    {
                        {tableName,writeRequestList }
                    };
                    await client.BatchWriteItemAsync(new BatchWriteItemRequest(dic));

                    i = 0;
                    writeRequestList = new List<WriteRequest>(50);
                }

                var writeRequest = new WriteRequest
                {
                    PutRequest = new PutRequest(new Dictionary<string, AttributeValue>
                    {
                        { "word", new AttributeValue(word)}
                    })
                };
                writeRequestList.Add(writeRequest);
            }
        }
    }
}