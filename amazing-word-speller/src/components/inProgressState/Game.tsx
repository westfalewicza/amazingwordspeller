import React from "react";
import './Game.css';
import '../../shared/shared.css'
import { AbstractWordComponentsProducer } from "../../services/producers/AbstractWordComponentsProducer";
import { Word } from "./Word";

interface GameProperties { 
    wordComponentsProducer: AbstractWordComponentsProducer, 
    submitWords: (words: string[], letters: string[]) => void,
    timeTolive: number
}

export class Game extends React.Component<GameProperties> {
    alreadyRendered:boolean=false;
    resultSubmitted:boolean=false;
    _words: string[] = [];
    _components: string[]=[];

    constructor(props: Readonly<GameProperties>) {
        super(props);
        this._components = this.props.wordComponentsProducer
            .getComponents()
    }

    handleKeyPress(event: React.KeyboardEvent<HTMLInputElement>) {
        if (event.key === 'Enter') {
            var input = event.target as HTMLInputElement;
            this._words.push(input.value);
            input.value="";
            this.forceUpdate();
        }
    }

    doneButtonClicked(event:React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        if(!this.resultSubmitted){
            this.resultSubmitted=true;
            this.props.submitWords(this._words,this._components)
        }
    }

    countDown(countDown: any, arg1: number) {
        if(!this.resultSubmitted){
            this.resultSubmitted=true;
            console.log("Time elapsed");
            this.props.submitWords(this._words,this._components);
        }
    }

    render() {
        if(!this.alreadyRendered){
            setTimeout(this.countDown.bind(this), 1000*this.props.timeTolive);
            this.alreadyRendered=true;
        }
        return (
            <div className="game-container">
                <div className="word-components-list">
                    {
                        this._components.map((value: string) => { return <p className="word-component">{value}</p> })
                    }
                </div>
                <div className="word-list">
                    {
                        this._words.map((value: string) => {return <Word word={value} letters={this._components}> </Word>})
                    }
                </div>
                <div className="word-input-container">
                    <input className="word-input" type="text" placeholder="hello there" onKeyPress={this.handleKeyPress.bind(this)} />
                </div>
                <button className="main-btn" onClick={this.doneButtonClicked.bind(this)} >Done</button>
            </div>
        );
    }
}
