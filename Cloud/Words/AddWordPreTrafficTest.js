"use strict";

const AWS = require("aws-sdk");
const codedeploy = new AWS.CodeDeploy({ apiVersion: "2014-10-06" });
const lambda = new AWS.Lambda();
const docClient = new AWS.DynamoDB.DocumentClient();

var deploymentEvent = undefined; 
var globalCallback = undefined;

var arrange = function(actCallback, assertCallback, cleanupCallback, reportStatusCallback) {
  console.log("ARRANGE");
  var params = {
    TableName: process.env.WordsTable,
    Key: { word: "KotyMajaMilusiomSierc" }
  };

  docClient.delete(params, function(err1, data) {
    if (err1) {
      console.log("ARRANGE ERROR : delete failed", err1);
      globalCallback(err1, null);
    } else {
      actCallback(assertCallback, cleanupCallback, reportStatusCallback);
    }
  });
};

var act = function(assertCallback, cleanupCallback, reportStatusCallback) {
  console.log("ACT");

  var args = {
    queryStringParameters: {
      email:"KociTester69@cat.org",
      password:"kotki"
    },
    pathParameters: {
      word: "KotyMajaMilusiomSierc"
    }
  };
  var lambdaParams = {
    FunctionName: process.env.FunctionToTest,
    InvocationType: "RequestResponse",
    Payload: JSON.stringify(args)
  };

  lambda.invoke(lambdaParams, function(err, lambdaResponse) {
    if (err) {
      console.log("LAMBDA FUNCTION EXECUTION FAILED");
      console.log(err, err.stack);
      globalCallback(err, null);
    } else {
      var result = JSON.parse(lambdaResponse.Payload);
      console.log("Result: " + JSON.stringify(lambdaResponse));
      assertCallback(cleanupCallback, reportStatusCallback, result);
    }
  });
};

var assert = function(cleanupCallback, reportStatusCallback, result) {
  console.log("ASSERT");
  if (result.statusCode != 200) {
    cleanupCallback(reportStatusCallback, "Failed");
  }
  var params = {
    TableName: process.env.WordsTable,
    Key: { word: "KotyMajaMilusiomSierc" }
  };

  docClient.get(params, function(err, data) {
    if (err) {
      console.log("ACT ERROR : GET failed", err);
      globalCallback(err, null);
    } else {
      if (data.Item.word != "KotyMajaMilusiomSierc") {
        console.log("ACT ERROR : ITEM WAS NOT CREATED", err);
        cleanupCallback(reportStatusCallback, "Failed");
      } else {
        cleanupCallback(reportStatusCallback, "Succeeded");
      }
    }
  });
};

var cleanup = function(reportStatusCallback, lambdaResult) {
  console.log("cleanup");
  var params = {
    TableName: process.env.WordsTable,
    Key: { word: "KotyMajaMilusiomSierc" }
  };

  docClient.delete(params, function(err1, data) {
    if (err1) {
      console.log("CLEANUP ERROR : delete failed", err1);
      globalCallback(err1, null);
    } else {
      reportStatusCallback(lambdaResult);
    }
  });
};

var reportStatus = function(lambdaResult) {
  console.log("reportStatus");

  var params = {
    deploymentId: deploymentEvent.DeploymentId,
    lifecycleEventHookExecutionId: deploymentEvent.LifecycleEventHookExecutionId,
    status: lambdaResult // status can be 'Succeeded' or 'Failed'
  };
  codedeploy.putLifecycleEventHookExecutionStatus(params, function(err, data) {
    if (err) {
      console.log("CodeDeploy Status update failed");
      console.log(err, err.stack);
      globalCallback("CodeDeploy Status update failed");
    } else {
      console.log("Codedeploy status updated successfully");
      console.log(data);
      globalCallback(null, "Codedeploy status updated successfully");
    }
  });
};

exports.handler = (event, context, callback) => {
  console.log("Entering PreTraffic Hook!");
  console.log('Received event:', JSON.stringify(event, null, 2));
  deploymentEvent = event;
  globalCallback = callback;
  arrange(act, assert, cleanup, reportStatus);
};
