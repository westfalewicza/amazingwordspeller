import axios from 'axios';

export class FakeApi {
    BASE_URL: string = 'https://g1retb47nc.execute-api.us-east-1.amazonaws.com/apiv1';

    authenticate(email: string, password: string): Promise<boolean> {
        return new Promise((resolve, reject)=> {
            setTimeout(() => {resolve(true);}, 300);
          });
    }

    checkInput(words: string[]): Promise<string[]> {
        console.log('checkInput');
        const temp = { words: words };
        return axios.post(`${this.BASE_URL}/words/validate`, temp).then(res => {
            console.log(res);
            return res.data;
        });
    }

    suggestWord(word: string): Promise<boolean> {
        console.log('suggestWord');
        return axios.post(`${this.BASE_URL}/word-suggestions/${word}`).then(res => {
            console.log(res);
            return true;
        });
    }

    getWordSuggestions(): Promise<string[]> {
        return new Promise((resolve, reject)=> {
            setTimeout(() => {resolve(['1','2','3','4','5','6']);}, 300);
          });
    }

    addWord(word: string): Promise<boolean> {
        console.log('addWord');
        return axios.post(`${this.BASE_URL}/words/${word}`).then(res => {
            console.log(res);
            return true;
        });
    }

    deleteWordSuggestion(word: string): Promise<boolean> {
        console.log('deleteWordSuggestion');
        return axios.delete(`${this.BASE_URL}/word-suggestions/${word}`).then(res => {
            console.log(res);
            return true;
        });
    }
}