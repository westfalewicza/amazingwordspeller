const AWS = require("aws-sdk");
var exitCallback = undefined;
const docClient = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });

    const htmlBody = `
        <!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
            <p>Hi,</p>
            <p>please review new words waiting for approval for the Amazing Word Speller Game.</p>
          </body>
        </html>
    `;
    
    const textBody = `
        Hi,
        please review new words waiting for approval for the Amazing Word Speller Game
    `;

exports.handler = function(event, context, callback) {
    exitCallback = callback;
    var wordsTableParams = {
        TableName: process.env.WordSuggestionsTable,
        Limit: 20
    };
    
    docClient.scan(wordsTableParams, function (err, wordsData) {
        if (err) {
            console.error("Error:", err);
            callback(err, null);
        } else {
            console.log("Success", wordsData);
            var count = 0;
            wordsData.Items.forEach(function (email) {
                count ++;
            });
            if (count > 0) {
                console.log("There are words to review - sending emails");
                SendMails(context);
            }
        }});
};

var SendMails = function (context) {
    var adminTableParams = {
        TableName: process.env.AdminsTable,
        Limit: 20
    };

    docClient.scan(adminTableParams, function (err, data) {
        if (err) {
            console.error("Error:", err);
            context.done(err, null);
            exitCallback(err,null);
        } else {
            console.log("Success", data);

            var emails = [];
            data.Items.forEach(function (item) {
                emails.push(item.email);
            });

            // Create sendEmail params
            const params = {
                Destination: {
                  ToAddresses: emails
                },
                Message: {
                  Body: {
                    Html: {
                      Charset: "UTF-8",
                      Data: htmlBody
                    },
                    Text: {
                      Charset: "UTF-8",
                      Data: textBody
                    }
                  },
                  Subject: {
                    Charset: "UTF-8",
                    Data: "Time to review new words!"
                  }
                },
                Source: "Amazing Word Speller <amazingwordspeller@gmail.com>"
            };
        
            // Create the promise and SES service object
            const sendPromise = new AWS.SES({ apiVersion: "2010-12-01" })
                .sendEmail(params)
                .promise();
            
            // Handle promise's fulfilled/rejected states
            sendPromise
                .then(data => {
                  console.log(data.MessageId);
                  context.done(null, "Success");
                  exitCallback(null, "Success");
                })
                .catch(err => {
                  console.error(err, err.stack);
                  context.done(null, "Failed");
                  exitCallback(err, null);
                });
        }
    });
}